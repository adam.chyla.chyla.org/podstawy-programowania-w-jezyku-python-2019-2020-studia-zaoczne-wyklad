
def czy_liczba(napis):
    dozwolone_znaki = '-0123456789.'
    for znak in napis:
        if znak in dozwolone_znaki:
            pass
        else:
            print("Błąd, niepoprawna liczba.")
            return False

    return True



# Wybierz operacje:

# 1. +
# 2. -
# 3. *
# 4. /
# Podaj x: xxxx
# Podaj y: yyyy

print('Wybierz operacje:')
print("1. +")
print("2. -")
print("3. *")
print("4. /")

operacja = input('Podaj operacje: ')
napis_x = input('Podaj x: ')

czy_liczba_x = czy_liczba(napis_x)

napis_y = input("Podaj y: ")

czy_liczba_y = czy_liczba(napis_y)

if czy_liczba_x is True and czy_liczba_y is True:
    x = float(napis_x)
    y = float(napis_y)

    if operacja == '+':
        print("Wynik:", x + y)
    elif operacja == '-':
        print("Wynik:", x - y)
    elif operacja == '*':
        print("Wynik:", x * y)
    elif operacja == '/':
        if y == 0:
            print("Błąd dzielenia!")
        else:
            print("Wynik:", x / y)
    else:
        print("Nieznana operacja.")
