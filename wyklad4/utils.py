def czy_liczba(napis):
    dozwolone_znaki = '-0123456789.'
    for znak in napis:
        if znak in dozwolone_znaki:
            pass
        else:
            print("Błąd, niepoprawna liczba.")
            return False

    return True

def suma(x):
    # x + ... + 3 + 2 + 1 + 0
    if x == 0:
        return 0
    else:
        return x + suma(x - 1)
