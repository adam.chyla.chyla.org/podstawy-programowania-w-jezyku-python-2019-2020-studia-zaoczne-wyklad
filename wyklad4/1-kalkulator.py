from utils import czy_liczba, suma

# Wybierz operacje:

# 1. +
# 2. -
# 3. *
# 4. /
# Podaj x: xxxx
# Podaj y: yyyy

print('Wybierz operacje:')
print("1. +")
print("2. -")
print("3. *")
print("4. /")
print("5. suma")

operacja = input('Podaj operacje: ')
napis_x = input('Podaj x: ')

napis_y = input("Podaj y: ")

if czy_liczba(napis_x) and czy_liczba(napis_y):
    x = float(napis_x)
    y = float(napis_y)

    if operacja == '+':
        print("Wynik:", x + y)
    elif operacja == '-':
        print("Wynik:", x - y)
    elif operacja == '*':
        print("Wynik:", x * y)
    elif operacja == '/':
        if y == 0:
            print("Błąd dzielenia!")
        else:
            print("Wynik:", x / y)
    elif operacja == 'suma':
        print("Wynik:", suma(x))
    else:
        print("Nieznana operacja.")
