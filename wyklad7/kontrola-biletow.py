import requests
from bs4 import BeautifulSoup

r = requests.get('http://mpk.wroc.pl/kontrole-biletow')
html = r.text

soup = BeautifulSoup(html, 'html.parser')

tytul_strony = soup.title.string
print(tytul_strony)
print('*' * len(tytul_strony))

wiersze = soup.find_all('tr')
wiersze = wiersze[1:]  # <-- slicing

for wiersz in wiersze:
    komorki = wiersz.find_all('td')

    data = komorki[0].text.strip()
    ulica = komorki[1].text.strip()
    linia = komorki[2].text.strip()

    out = f"{data}\t{ulica}  -->  {linia}"
    print(out)



