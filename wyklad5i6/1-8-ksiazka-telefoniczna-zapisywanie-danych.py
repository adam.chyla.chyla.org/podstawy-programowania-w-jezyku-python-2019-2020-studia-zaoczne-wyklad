# 1. wypisanie ksiazki
# 2. dodanie
# 3. wyjscie z programu
# wybor (1/2/3):

ksiazka = {}

with open("dane.txt") as file:
    for line in file:
        # print("linia z pliku:", line)
        pola = line.strip().split(";")
        # print(pola)
        ksiazka[pola[0]] = pola[1]

wybor = None
while wybor != '3':
    print('1. wypisz ksiazke tel.')
    print('2. dodaj kogos do ksiazki')
    print('3. wyjscie')
    wybor = input('wybor (1/2/3):')

    if wybor == '1':
        print("Zawartosc ksiazki telefonicznej")
        for nr_wiersza, x in enumerate(ksiazka):
            print(f"{nr_wiersza+1}. {x}\t{ksiazka[x]}")
    elif wybor == '2':
        imie = input('Podaj imie: ')
        tel = input('Podaj numer: ')

        ksiazka[imie] = tel
    elif wybor == '3':
        print('Koncze dzialanie')
        with open("dane.txt", "w") as file:
            for imie in ksiazka:
                nr_telefonu = ksiazka[imie]
                linia = f"{imie};{nr_telefonu}\n"
                file.write(linia)
    else:
        print("Blad: niepoprawna opcja:", wybor)
