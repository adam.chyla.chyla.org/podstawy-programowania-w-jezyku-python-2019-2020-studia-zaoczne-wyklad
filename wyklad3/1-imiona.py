
# lista imion
imiona = ['Alicja',   # 0
          'Mateusz',  # 1
          'Jan',      # 2
          'Kasia']    # 3

print(imiona[0])

# ile elementów
print("liczba elementów listy",
      len(imiona))

# dodać kolejny element
imiona.append('Magda')

print(imiona)

# usunięcie elementu
del(imiona[1])

print('usuniety element',
      imiona)

# zamiana elementu
imiona[2] = 'Agata'

print('zamiana Kasia -> Agata')
print(imiona)

print('petla for')
for x in imiona:
    print(x)
    print('Czesc', x)

# operator indeksowania
for i in range(0, len(imiona)):
    print(i)
    print(imiona[i])

print("petla while")

x = 0
while x < len(imiona):
    print(x)
    print(imiona[x])
    x = x + 1
